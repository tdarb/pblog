---
title: Announcing pblog
date: Thu, 30 Jun 2022
---

It's time to celebrate! The official launch of `pblog` happened today.

There isn't much else to say. All details and instructions on how to start your own pblog can be found on the [main homepage for this project](https://pblog.xyz).

Thanks for reading!

