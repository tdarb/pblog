#!/bin/sh

# Site specific settings
###################################################################################
DOMAIN="https://pblog.xyz"
TITLE="pblog.xyz"
DESCRIPTION="Pandoc static blog generator"
COPYRIGHT="Copyright 2022, Bradley Taunt"
AUTHOR="hello@tdarb.org (Bradley Taunt)"
OS="BSD" # "Linux" for Linux, "BSD" for BSD Systems (including MacOS)
HTML_LANG="en_US" # Your document (HTML) language setting

# Blog structure settings (most users should use these defaults)
###################################################################################
TOC=true
SYNTAX=true
PAGES_DIR="pages/"
POSTS_DIR="posts/"
PAGES=$(find $PAGES_DIR -type f)
POSTS=$(find $POSTS_DIR -type f)
WEB_HTML="blog/"
OUTPUT="_output/"
TIME=$(date +"%T %Z")
TTL="60"
