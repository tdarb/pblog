# pblog

Pandoc static blog generator

[https://pblog.xyz](https://pblog.xyz)

## Getting Started

You can always read through the up-to-date article: [Introducing pblog](https://pblog.xyz/blog/pblog-intro.html)

But very basic steps:

1. Install dependencies
2. Write posts in the `posts` directory
3. Write pages in the `pages` directory
4. Build the site
5. Upload your `_output` folder to your web host

Enjoy!
